FROM centos:8

WORKDIR /exec
ADD prometheus-am-executor .
ADD executor.bash .
CMD ["/exec/prometheus-am-executor", "executor.bash" ]
